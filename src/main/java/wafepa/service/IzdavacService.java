package wafepa.service;

import java.util.List;

import org.springframework.data.domain.Page;

import wafepa.model.Izdavac;

public interface IzdavacService {
	
	Izdavac findOne(Long id);
	
	Page<Izdavac>  findAll(int pageNum);
	List<Izdavac> findAll();
	
	Izdavac save(Izdavac izdavac);
	
//	List<ModelJedan> save(List<ModelJedan> modeli);
	
	Izdavac delete(Long id);
	
//	Page<Record> search(
//	@Param("activityName") String activityName, 
//	@Param("minDuration") Integer minDuration, 
//	@Param("intensity") String intensity,
//	 int pageNum);

//  Page<Automobil> pretraga(@Param("model")String model,
//       @Param("godisteOd")Integer godisteOd,
//  @Param("potrosnjaDo")Double potrosnjaDo, 
//  int pageNum);

//  List<Zadatak> findBySprintId(Long sprintID);	
//  Page<Record> findByUserId(Long id, int pageNum);
//  List<Address> findByUser(Long userId);
}
