package wafepa.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import wafepa.model.Knjiga;

public interface KnjigaService {

	Knjiga findOne(Long id);
	
	Page<Knjiga> findAll(int pageNum);
	List<Knjiga> findAll();
	
	Knjiga save(Knjiga knjiga);
	
//	List<ModelVise> save(List<ModelVise> modeli);

    Knjiga delete(Long id);

	List<Knjiga> findByIzdavacId(Long id);

	Page<Knjiga> search(
			@Param("naziv")String naziv, 
			@Param("pisac")String pisac,
			@Param("minGlasova")Integer minGlasova,
			int pageNum);

	void povecajBrojGlasova(Knjiga knjiga);

	
	Page<Knjiga> nadjiOmiljenu(int pageNum);
	
	Knjiga nadjiFavorit();
    
//	Page<Record> search(
//			@Param("activityName") String activityName, 
//			@Param("minDuration") Integer minDuration, 
//			@Param("intensity") String intensity,
//			 int pageNum);
    
//	Page<Automobil> pretraga(@Param("model")String model,
//		       @Param("godisteOd")Integer godisteOd,
//	      @Param("potrosnjaDo")Double potrosnjaDo, 
//	  int pageNum);
    
//    List<Zadatak> findBySprintId(Long sprintID);	
//    Page<Record> findByUserId(Long id, int pageNum);
//    List<Address> findByUser(Long userId);
}
