package wafepa.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wafepa.model.Glas;
import wafepa.repository.GlasRepository;
import wafepa.service.GlasService;

@Service
@Transactional
public class JpaGlasService implements GlasService{
	
	@Autowired
	GlasRepository glasRep;

	@Override
	public Glas save(Glas glas) {
		return glasRep.save(glas);
	}

}
