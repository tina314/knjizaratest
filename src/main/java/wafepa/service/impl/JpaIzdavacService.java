package wafepa.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import wafepa.model.Izdavac;
import wafepa.repository.IzdavacRepository;
import wafepa.service.IzdavacService;

@Service
@Transactional
public class JpaIzdavacService implements IzdavacService{

	@Autowired
	private IzdavacRepository izdavacRep;
	
	
	@Override
	public Izdavac findOne(Long id) {
		return izdavacRep.findOne(id);
	}

	@Override
	public Page<Izdavac> findAll(int pageNum) {
		return izdavacRep.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public List<Izdavac> findAll() {
		return izdavacRep.findAll();
	}

	@Override
	public Izdavac save(Izdavac entitet) {
		return izdavacRep.save(entitet);
	}

	@Override
	public Izdavac delete(Long id) {
		Izdavac model = izdavacRep.findOne(id);
		if (model == null) {
			throw new IllegalArgumentException("Pokusaj brisanja "
					+ "nepostojeceg izdavaca");
		}
		izdavacRep.delete(id);
		return model;
	}
	
//	@Override
//	public Page<Record> search( String activityName, Integer minDuration,
//								String intensity, int pageNum){
//		
//		if(activityName != null) {
//			activityName = '%' + activityName + '%';
//		}
//		if(intensity != null) {
//			intensity = '%' + intensity + '%';
//		}
//		
//		return recordRepository.search(activityName, minDuration, intensity, new PageRequest(pageNum, 5));
//	}
	
//	@Override
//	public Page<Automobil> pretraga(String model, Integer godisteOd, Double potrosnjaDo, int pageNum) {
//		if (model != null) {
//			model = "%" + model + "%";
//		}
//		return autoRep.pretraga(model, godisteOd, potrosnjaDo, new PageRequest(pageNum, 5));
//	}

//	@Override
//	public Page<Record> findByUserId(Long id, int pageNum) {
//		return recordRepository.findByUserId(id, new PageRequest(pageNum, 5));
//	}
}
