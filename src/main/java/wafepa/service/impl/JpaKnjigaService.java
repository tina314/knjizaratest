package wafepa.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import wafepa.model.Glas;
import wafepa.model.Knjiga;
import wafepa.repository.KnjigaRepository;
import wafepa.service.GlasService;
import wafepa.service.KnjigaService;

@Service
@Transactional
public class JpaKnjigaService implements KnjigaService{

	@Autowired
	private KnjigaRepository knjigaRep;
	
	@Autowired
	private GlasService glasService;
	
	@Override
	public Knjiga findOne(Long id) {
		return knjigaRep.findOne(id);
	}

	@Override
	public Page<Knjiga> findAll(int pageNum) {
		return knjigaRep.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public List<Knjiga> findAll() {
		return knjigaRep.findAll();
	}

	@Override
	public Knjiga save(Knjiga entitet) {
		return knjigaRep.save(entitet);
	}

	@Override
	public Knjiga delete(Long id) {
		Knjiga model = knjigaRep.findOne(id);
		if (model == null) {
			throw new IllegalArgumentException("Pokusaj brisanja "
					+ "nepostojeceg knjige");
		}
		knjigaRep.delete(id);
		return model;
	}

	@Override
	public List<Knjiga> findByIzdavacId(Long id) {
		return knjigaRep.findByIzdavacId(id);
	}

	@Override
	public Page<Knjiga> search(String naziv, String pisac, Integer minGlasova, int pageNum) {
		if (naziv != null) {
			naziv = "%" + naziv + "%";
		}
		if (pisac != null) {
			pisac = "%" + pisac + "%";
		}
		return knjigaRep.search(naziv, pisac, minGlasova, new PageRequest(pageNum, 5));
	}

	@Override
	public void povecajBrojGlasova(Knjiga knjiga) {
		knjiga.setBrojGlasova(knjiga.getBrojGlasova() + 1);
		this.save(knjiga);
		
		Glas glas = new Glas();
		glas.setKnjiga(knjiga);
		glasService.save(glas);
	}

	@Override
	public Page<Knjiga> nadjiOmiljenu(int pageNum) {
	//	return knjigaRep.findTopByMaxBrojGlasova(); ///neeee
		return knjigaRep.nadjiOmiljenu(new PageRequest(pageNum, 1));
	}
	
	public Knjiga nadjiFavorit() {
		//return knjigaRep.findFirstByOrderByBrojGlasovaDesc();
		return knjigaRep.findTopByOrderByBrojGlasovaDesc();
	}

//	@Override
//	public Page<Automobil> pretraga(String model, Integer godisteOd, Double potrosnjaDo, int pageNum) {
//		if (model != null) {
//			model = "%" + model + "%";
//		}
//		return autoRep.pretraga(model, godisteOd, potrosnjaDo, new PageRequest(pageNum, 5));
//	}
	
	
//	@Override
//	public Page<Record> search( String activityName, Integer minDuration,
//								String intensity, int pageNum){
//		
//		if(activityName != null) {
//			activityName = '%' + activityName + '%';
//		}
//		if(intensity != null) {
//			intensity = '%' + intensity + '%';
//		}
//		
//		return recordRepository.search(activityName, minDuration, intensity, new PageRequest(pageNum, 5));
//	}

//	@Override
//	public Page<Record> findByUserId(Long id, int pageNum) {
//		return recordRepository.findByUserId(id, new PageRequest(pageNum, 5));
//	}
}
