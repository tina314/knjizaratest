package wafepa.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import wafepa.dto.IzdavacDTO;
import wafepa.model.Izdavac;
import wafepa.service.IzdavacService;

@Component
public class IzdavacDtoToIzdavac implements Converter<IzdavacDTO, Izdavac>{

	@Autowired
	private IzdavacService izdavacService;
	
	
	@Override
	public Izdavac convert(IzdavacDTO dto) {
		
		Izdavac izdavac = null;
		
		if (dto.getId() == null) {
			izdavac = new Izdavac();
		}else {
			izdavac = izdavacService.findOne(dto.getId());
			if (izdavac == null) {
				throw new IllegalStateException("Pokusaj konvertovanja "
						+ "nepostojeceg izdavaca");
			}
		}
		
		izdavac.setId(dto.getId());  //mislim da ovo ne treba
		izdavac.setAdresa(dto.getAdresa());
		izdavac.setNaziv(dto.getNaziv());
		izdavac.setTelefon(dto.getTelefon());
		
		return izdavac;
	}
	
	public List<Izdavac> convert(List<IzdavacDTO> dtos){
		
		List<Izdavac> ret = new ArrayList<>();
		
		for (IzdavacDTO dto : dtos) {
			ret.add(convert(dto));
		}
		
		return ret;
	}

}
