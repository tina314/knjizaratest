package wafepa.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import wafepa.dto.IzdavacDTO;
import wafepa.model.Izdavac;

@Component
public class IzdavacToIzdavacDTO 
				implements Converter<Izdavac, IzdavacDTO>{

	@Override
	public IzdavacDTO convert(Izdavac source) {
		
		if (source == null) {
			return null;
		}
		
		IzdavacDTO dto = new IzdavacDTO();
		
		dto.setId(source.getId());
		dto.setAdresa(source.getAdresa());
		dto.setNaziv(source.getNaziv());
		dto.setTelefon(source.getTelefon());
		
		return dto;
	}
	
	public List<IzdavacDTO> convert(List<Izdavac> lista){
		List<IzdavacDTO> ret = new ArrayList<>();
		
		for(Izdavac s : lista){
			ret.add(convert(s));
		}
		
		return ret;
	}

}
