package wafepa.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import wafepa.dto.KnjigaDTO;
import wafepa.model.Izdavac;
import wafepa.model.Knjiga;
import wafepa.service.IzdavacService;
import wafepa.service.KnjigaService;

@Component
public class KnjigaDtoToKnjiga implements Converter<KnjigaDTO, Knjiga> {

	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private IzdavacService izdavacService;

	@Override
	public Knjiga convert(KnjigaDTO dto) {

		 Izdavac izdavac = izdavacService.findOne(dto.getIzdavacID());

		if (izdavac != null) {

			Knjiga knjiga = null;

			if (dto.getId() != null) {
				knjiga = knjigaService.findOne(dto.getId());
				
				if (knjiga == null) {
					throw new IllegalStateException("Tried to "
							+ "modify a non-existant entity");
				}
			} else {
				knjiga = new Knjiga();
			}

			knjiga.setId(dto.getId());
			knjiga.setBrojGlasova(dto.getBrojGlasova());
			knjiga.setIsbn(dto.getIsbn());
			knjiga.setIzdanje(dto.getIzdanje());
			knjiga.setNaziv(dto.getNaziv());
			knjiga.setPisac(dto.getPisac());
			
			knjiga.setIzdavac(izdavac);

			return knjiga;
		} else {
			throw new IllegalStateException("Trying to attach to non-existant entities");
		}
	}
	
	
	public List<Knjiga> convert(List<KnjigaDTO> dtos){
		List<Knjiga> ret = new ArrayList<>();
		
		for(KnjigaDTO a : dtos){
			ret.add(convert(a));
		}
		
		return ret;
	}

}
