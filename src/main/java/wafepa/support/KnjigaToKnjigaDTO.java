package wafepa.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import wafepa.dto.KnjigaDTO;
import wafepa.model.Knjiga;

@Component
public class KnjigaToKnjigaDTO implements Converter<Knjiga, KnjigaDTO>{

	@Override
	public KnjigaDTO convert(Knjiga s) {
		
		if (s == null) {
			return null;
		}
		
		KnjigaDTO dto = new KnjigaDTO();
		
		dto.setId(s.getId());
		dto.setBrojGlasova(s.getBrojGlasova());
		dto.setIsbn(s.getIsbn());
		dto.setIzdanje(s.getIzdanje());
		dto.setNaziv(s.getNaziv());
		dto.setPisac(s.getPisac());
		
		dto.setIzdavacNaziv(s.getIzdavac().getNaziv());
		dto.setIzdavacID(s.getIzdavac().getId());

		return dto;
	}
	
	public List<KnjigaDTO> convert(List<Knjiga> lista){
		List<KnjigaDTO> ret = new ArrayList<>();
		
		for(Knjiga k : lista){
			ret.add(convert(k));
		}
		
		return ret;
	}
}
