package wafepa;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import wafepa.model.Izdavac;
import wafepa.model.Knjiga;
import wafepa.service.IzdavacService;
import wafepa.service.KnjigaService;

@Component
public class TestData {
	
	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private IzdavacService izdavacService;
	
	@PostConstruct
	public void init() {
		
		Izdavac i1 = new Izdavac();
		i1.setAdresa("Zmaj Jovina 24");
		i1.setNaziv("Vulkan");
		i1.setTelefon("021/423-7515");
		i1 = izdavacService.save(i1);
		
		Izdavac i2 = new Izdavac();
		i2.setAdresa("Trg Marije Trandafil 11");
		i2.setNaziv("Prometej");
		i2.setTelefon("021/616-018");
		i2 = izdavacService.save(i2);
		
		Knjiga k1 = new Knjiga();
		k1.setBrojGlasova(22);
		k1.setIsbn("2302001578411");
		k1.setIzdanje(2004);
		k1.setNaziv("Inferno");
		k1.setPisac("Den Braun");
		k1.setIzdavac(i1);
		knjigaService.save(k1);
		
		Knjiga k2 = new Knjiga();
		k2.setBrojGlasova(157);
		k2.setIsbn("6635896415875");
		k2.setIzdanje(2000);
		k2.setNaziv("Hari Poter");
		k2.setPisac("Dz.K.Rouling");
		k2.setIzdavac(i1);
		knjigaService.save(k2);
		
		Knjiga k3 = new Knjiga();
		k3.setBrojGlasova(120);
		k3.setIsbn("3652145225412");
		k3.setIzdanje(1999);
		k3.setNaziv("Gospodar prstenova");
		k3.setPisac("Dz.R.R.Tolkin");
		k3.setIzdavac(i2);
		knjigaService.save(k3);
		
		Knjiga k4= new Knjiga();
		k4.setBrojGlasova(22);
		k4.setIsbn("2586593655211");
		k4.setIzdanje(2004);
		k4.setNaziv("Digitalna tvrdjava");
		k4.setPisac("Den Braun");
		k4.setIzdavac(i1);
		knjigaService.save(k4);
		
	
	}
}
