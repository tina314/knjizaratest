package wafepa.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import wafepa.model.Knjiga;

@Repository
public interface KnjigaRepository extends JpaRepository<Knjiga, Long> {

	List<Knjiga> findByIzdavacId(Long id);
	
	@Query("SELECT k FROM Knjiga k WHERE "
	+ "(:naziv IS NULL or k.naziv like :naziv ) AND "
	+ "(:minGlasova IS NULL OR k.brojGlasova >= :minGlasova) AND "
	+ "(:pisac IS NULL or k.pisac like :pisac) "
	)
	Page<Knjiga> search(
			@Param("naziv")String naziv, 
			@Param("pisac")String pisac, 
			@Param("minGlasova")Integer minGlasova, 
			Pageable pageRequest);

	@Query("SELECT k FROM Knjiga k WHERE "
			+ "k.brojGlasova = (SELECT MAX(k.brojGlasova) FROM Knjiga k)"
			)	
	Page<Knjiga> nadjiOmiljenu(Pageable pageRequest); //1. nacin
	
	Knjiga findFirstByOrderByBrojGlasovaDesc();  //2. nacin
	
	Knjiga findTopByOrderByBrojGlasovaDesc();  //3. nacin
	
}
