package wafepa.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import wafepa.model.Izdavac;

@Repository
public interface IzdavacRepository extends JpaRepository<Izdavac, Long>{

	
//	Page<Record> findByUserId(Long id, Pageable page);
//	
//	List<Address> findByUserId(Long userId);
//	
//	@Query("SELECT r FROM Record r WHERE "
//			+ "(:activityName IS NULL or r.activity.name like :activityName ) AND "
//			+ "(:minDuration IS NULL OR r.duration >= :minDuration) AND "
//			+ "(:intensity IS NULL or r.intensity like :intensity ) "
//			)
//	Page<Record> search(
//			@Param("activityName") String activityName, 
//			@Param("minDuration") Integer minDuration, 
//			@Param("intensity") String intensity,
//			Pageable pageRequest);
	//Pageable, not PageRequest!
	
	
//	@Query("SELECT a from Automobil a WHERE "
//			+ "(:model IS NULL OR a.model LIKE :model) AND "
//			+ " (:godisteOd IS NULL OR a.godiste >= :godisteOd) AND "
//			+ " (:potrosnjaDo IS NULL OR a.potrosnja <= :potrosnjaDo)"
//				)
//	Page<Automobil> search(@Param("model")String model, 
//			@Param("godisteOd")Integer godisteOd,
//		@Param("potrosnjaDo")Double potrosnjaDo, 
//	Pageable pageRequest);
}
