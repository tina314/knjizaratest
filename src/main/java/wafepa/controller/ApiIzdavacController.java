package wafepa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import wafepa.dto.IzdavacDTO;
import wafepa.dto.KnjigaDTO;
import wafepa.model.Izdavac;
import wafepa.model.Knjiga;
import wafepa.service.IzdavacService;
import wafepa.service.KnjigaService;
import wafepa.support.IzdavacDtoToIzdavac;
import wafepa.support.IzdavacToIzdavacDTO;
import wafepa.support.KnjigaToKnjigaDTO;

@Controller
@RequestMapping(value = "/api/izdavaci")
public class ApiIzdavacController {

	@Autowired
	private IzdavacService izdavacService;

	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private IzdavacDtoToIzdavac toIzdavac;

	@Autowired
	private IzdavacToIzdavacDTO toDto;
	
	@Autowired
	private KnjigaToKnjigaDTO toKnjigaDto;

	
//	@RequestMapping(method = RequestMethod.GET)
//	ResponseEntity<List<IzdavacDTO>> getIzdavaci(){
//		
//		List<Izdavac> lista = izdavacService.findAll();
//		
//		return new ResponseEntity<>(toDto.convert(lista), HttpStatus.OK);
//	}
	
	
//	SA PAGINACIJOM I PRETRAGOM
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<IzdavacDTO>> getIzdavaci(
			@RequestParam(value = "pageNum", defaultValue = "0") int pageNum) {

		Page<Izdavac> stranica = izdavacService.findAll(pageNum);	

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(stranica.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(stranica.getContent()), headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<IzdavacDTO> getIzdavac(@PathVariable Long id) {
		Izdavac izdavac = izdavacService.findOne(id);
		if (izdavac == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(toDto.convert(izdavac), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<IzdavacDTO> delete(@PathVariable Long id) throws Exception {
		
		Izdavac izdavac = izdavacService.findOne(id);
		
		if (izdavac == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (izdavac.getNaziv().equals("Vulkan") || izdavac.getNaziv().equals("Prometej")) {
			throw new Exception("Zabranjeno brisanje izvodjaca.");
		}
		
		Izdavac deleted = izdavacService.delete(id);

		if (deleted == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDto.convert(deleted), HttpStatus.OK);
	}
//
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<IzdavacDTO> add(@Validated @RequestBody IzdavacDTO dto) {
		
		Izdavac saved = izdavacService.save(toIzdavac.convert(dto));

		return new ResponseEntity<>(toDto.convert(saved), HttpStatus.CREATED);
	}
//
//	
//	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
//	public ResponseEntity<IzdavacDTO> edit(@Validated @RequestBody IzdavacDTO dto, @PathVariable Long id) {
//
//		if (!id.equals(dto.getId())) {
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		}
//
//		Izdavac persisted = izdavacService.save(toModelJedan.convert(dto));
//
//		return new ResponseEntity<>(toDto.convert(persisted), HttpStatus.OK);
//	}
	

	@RequestMapping(value= "/{id}/knjige", method=RequestMethod.GET)
	ResponseEntity<List<KnjigaDTO>> getKnjigeByIzdavac(
			@PathVariable Long id){
		
		Izdavac izdavac = izdavacService.findOne(id);
		
		if (izdavac == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		List<Knjiga> knjige = knjigaService.findByIzdavacId(id);
		
		if(knjige == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(
				toKnjigaDto.convert(knjige),
				HttpStatus.OK);
	}
	
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
