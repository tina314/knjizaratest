package wafepa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import wafepa.dto.KnjigaDTO;
import wafepa.model.Knjiga;
import wafepa.service.KnjigaService;
import wafepa.support.KnjigaDtoToKnjiga;
import wafepa.support.KnjigaToKnjigaDTO;

@RestController
@RequestMapping(value = "/api/knjige")
public class ApiKnjigaController {

	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private KnjigaDtoToKnjiga toKnjiga;

	@Autowired
	private KnjigaToKnjigaDTO toDto;

	
//	@RequestMapping(method = RequestMethod.GET, value = "/favorit")  //primer sa Page
//	ResponseEntity<List<KnjigaDTO>> getFavoritKnjigu(){
//		
//		List<Knjiga> lista = knjigaService.findAll();
//		
//		if (lista.isEmpty() || lista == null) {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//		
//		
//		Page<Knjiga> favorit = knjigaService.nadjiOmiljenu(0);
//		
//		return new ResponseEntity<>(toDto.convert(favorit.getContent()), HttpStatus.OK);
//	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/favorit")
	ResponseEntity<KnjigaDTO> getFavoritKnjigu() throws Exception{
		
		List<Knjiga> lista = knjigaService.findAll();
		
		if (lista.isEmpty() || lista == null) {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			throw new Exception("Lista knjiga je prazna pa ne postoji knjiga sa najvise glasova");
		}
		
		Knjiga favorit = knjigaService.nadjiFavorit();
		
		return new ResponseEntity<>(toDto.convert(favorit), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<List<KnjigaDTO>> getKnjige(
			@RequestParam(required=false, value = "naziv") String naziv,
			@RequestParam(required=false, value = "pisac") String pisac,
			@RequestParam(required=false, value = "minGlasova") Integer minGlasova,
			@RequestParam(value = "pageNum", defaultValue = "0") int pageNum) {

		Page<Knjiga> stranica = null;
				
			if (naziv != null || pisac != null || minGlasova != null) {
				stranica = knjigaService.search(naziv, pisac, minGlasova, pageNum);
			} else {
				stranica = knjigaService.findAll(pageNum);
			}	

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(stranica.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(stranica.getContent()), headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<KnjigaDTO> getKnjiga(@PathVariable Long id) {
		Knjiga knjiga = knjigaService.findOne(id);
		if (knjiga == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(toDto.convert(knjiga), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	ResponseEntity<KnjigaDTO> delete(@PathVariable Long id) {
		Knjiga deleted = knjigaService.delete(id);

		if (deleted == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDto.convert(deleted), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<KnjigaDTO> add(@Validated @RequestBody KnjigaDTO dto) {
		
		if (dto.getBrojGlasova() == null || !dto.getBrojGlasova().equals(0)) {
			dto.setBrojGlasova(0);
		}

		Knjiga saved = knjigaService.save(toKnjiga.convert(dto));

		return new ResponseEntity<>(toDto.convert(saved), HttpStatus.CREATED);
	}

	
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<KnjigaDTO> edit(@Validated @RequestBody KnjigaDTO dto, @PathVariable Long id) {

		if (!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Knjiga persisted = knjigaService.save(toKnjiga.convert(dto));

		return new ResponseEntity<>(toDto.convert(persisted), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/glasanje", consumes = "application/json")
	public ResponseEntity<Void> glasaj(@Validated @RequestBody Long dtoID, @PathVariable Long id) {

		if (!id.equals(dtoID)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Knjiga knjiga = knjigaService.findOne(id);
		
		if (knjiga == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		knjigaService.povecajBrojGlasova(knjiga);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
