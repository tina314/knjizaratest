package wafepa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Izdavac {

	@Id
	@GeneratedValue
	private Long id;
	private String naziv;
	private String adresa;
	private String telefon;

	@OneToMany(mappedBy = "izdavac", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Knjiga> knjige = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public List<Knjiga> getKnjige() {
		return knjige;
	}

	public void setKnjige(List<Knjiga> knjige) {
		this.knjige = knjige;
	}
	
		
	public void addKnjiga(Knjiga knjiga) {
		if(knjiga.getIzdavac() != this) {
			knjiga.setIzdavac(this);
		}
		knjige.add(knjiga);
	}
}
