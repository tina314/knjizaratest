package wafepa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Knjiga {

	@Id
	@GeneratedValue
	private Long id;
	@Column(nullable = false)
	private String naziv;
	@Column(nullable = false)
	private Integer izdanje;
	@Column(nullable = false)
	private String pisac;
	@Column(nullable = false, unique = true)
	private String isbn;
	private Integer brojGlasova;
	
	@OneToMany(mappedBy = "knjiga")
	private List<Glas> glasovi = new ArrayList<Glas>();
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Izdavac izdavac;
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	
	
	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public Integer getIzdanje() {
		return izdanje;
	}


	public void setIzdanje(Integer izdanje) {
		this.izdanje = izdanje;
	}


	public String getPisac() {
		return pisac;
	}


	public void setPisac(String pisac) {
		this.pisac = pisac;
	}


	public String getIsbn() {
		return isbn;
	}


	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	public Integer getBrojGlasova() {
		return brojGlasova;
	}


	public void setBrojGlasova(Integer brojGlasova) {
		this.brojGlasova = brojGlasova;
	}


	public Izdavac getIzdavac() {
		return izdavac;
	}


	public void setIzdavac(Izdavac izdavac) {
		this.izdavac = izdavac;
		if (!izdavac.getKnjige().contains(this)) {
			izdavac.getKnjige().add(this);
		}
	}


	public List<Glas> getGlasovi() {
		return glasovi;
	}


	public void setGlasovi(List<Glas> glasovi) {
		this.glasovi = glasovi;
	}
	
	public void addGlas(Glas glas) {
		if (glas.getKnjiga() != this) {
			glas.setKnjiga(this);
		}
		glasovi.add(glas);
	}
}
