var wafepaApp = angular.module("wafepaApp",["ngRoute"]);

wafepaApp.controller("KnjigeCtrl", function($scope, $http, $location){
	
	var urlKnjige = "/api/knjige";
	var urlIzdavaci = "/api/izdavaci";
	
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	$scope.knjige = [];
	
	$scope.newKnjiga = {};
	$scope.newKnjiga.naziv = "";
	$scope.newKnjiga.izdanje = "";
	$scope.newKnjiga.pisac = "";
	$scope.newKnjiga.isbn = "";
	$scope.newKnjiga.izdavacID = "";
	$scope.newKnjiga.brojGlasova = 0;
		
	$scope.search = {};
	$scope.search.naziv = "";
	$scope.search.pisac = "";
	$scope.search.minGlasova = "";
	
	$scope.izdavaci = [];
	$scope.izdavac = {};
	$scope.izdavac.naziv = "";
	
	var getIzdavaci= function(){
		var promise = $http.get(urlIzdavaci);
		promise.then(
			function success(res){
				$scope.izdavaci = res.data;
			},
			function error(res){
				alert("Neuspesno dobavljanje izdavaca.");
			}
		);
	}
	
	getIzdavaci();

	var getKnjige = function(){
		
		var config = {params: {}};
		if($scope.search.naziv != ""){
			config.params.naziv = $scope.search.naziv;
		}
		if($scope.search.pisac != ""){
			config.params.pisac = $scope.search.pisac;
		}
		if($scope.search.minGlasova != ""){
			config.params.minGlasova = $scope.search.minGlasova;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(urlKnjige, config);
		promise.then(
			function success(res){
				$scope.knjige = res.data;
				$scope.totalPages = res.headers("totalPages");
			},
			function error(){
				alert("Neuspesno dobavljanje liste knjiga");
			}
		);
	}
	
	getKnjige();
	
	$scope.doSearch = function(){
		$scope.pageNum = 0;
		getKnjige();
	}

	$scope.changePage = function(direction){
		$scope.pageNum += direction;
		getKnjige();
	}
	
	$scope.goToEdit = function(id){
		$location.path("/knjiga/edit/" + id);
	}

	$scope.doDelete = function(id){
		var promise = $http.delete(urlKnjige + "/" + id);
		promise.then(
			function success(){
				getKnjige();
				prikaziFavorit();
			},
			function error(){
				alert("Neuspesno brisanje knjige.");
			}
		);
	}

	$scope.doAdd = function(){
		
		$http.post(urlKnjige, $scope.newKnjiga).then(
			function success(res){
				$scope.newKnjiga = {};
				getKnjige();
				prikaziFavorit();
			},
			function error(){
				alert("Knjiga nije sacuvana.");
			}
		);
	}
		
	$scope.knjiga = {};  
	
	$scope.dodajGlas = function(id){
		
		var promise = $http.get(urlKnjige + "/" + id);
		promise.then(
			function uspeh(odg){
				$scope.knjiga = odg.data;
				
				var promise = $http.put(urlKnjige + "/" + id + "/glasanje", $scope.knjiga.id);
				promise.then(
					function success(res){
						getKnjige();
						prikaziFavorit();
					},
					function error(){
						alert("Nesupesna izmena knjige u toku glasanja");
					}
				);
				
			},
			function neuspeh(odg){
				alert("Neuspesno dobavljanje knjige.");
			}
		);
	}
	
//	$scope.favKnjige = [];  //za slucaj Page i List
	$scope.favKnjiga = {};
	
	var prikaziFavorit = function(){
		
		var promise = $http.get(urlKnjige + "/favorit");
		promise.then(
			function success(res){
			//	$scope.favKnjige = res.data;
			//	$scope.favKnjiga = $scope.favKnjige[0];
				$scope.favKnjiga = res.data;
			},
			function error(){
				alert("Neuspesno dobavljanje knjige sa najvecim brojem glasova.");
			}
		);
	}
	
	prikaziFavorit();
	
});


wafepaApp.controller("EditKnjigaCtrl", function($scope, $http, $routeParams, $location){
	
	var urlKnjiga = "/api/knjige/" + $routeParams.id;
	var urlIzdavaci = "/api/izdavaci";
	
	$scope.knjiga = {};
	$scope.knjiga.naziv = "";
	$scope.knjiga.izdanje = "";
	$scope.knjiga.pisac = "";
	$scope.knjiga.isbn = "";
	$scope.knjiga.izdavacID = "";
	
	var getKnjiga = function(){
		var promise = $http.get(urlKnjiga);
		promise.then(
			function uspeh(odg){
				$scope.knjiga = odg.data;
			},
			function neuspeh(odg){
				alert("Neuspesno dobavljanje knjige.");
			}
		);
	}
	
	getKnjiga();

	$scope.doEdit = function(){
		var promise = $http.put(urlKnjiga, $scope.knjiga);
		promise.then(
			function success(res){
				$location.path("/knjige");
			},
			function error(){
				alert("Neuspesna izmena knjige.");
			}
		);
	}
	
	var getIzdavaci= function(){
		var promise = $http.get(urlIzdavaci);
		promise.then(
			function success(res){
				$scope.izdavaci = res.data;
			},
			function error(res){
				alert("Neuspesno dobavljanje izdavaca.");
			}
		);
	}
	
	getIzdavaci();
	
});

wafepaApp.controller("IzdavaciCtrl", function($scope, $http, $location){
	
	var urlIzdavaci = "/api/izdavaci";
	
	$scope.izdavaci = [];
	$scope.izdavac = {};
	$scope.izdavac.naziv = "";
	$scope.izdavac.adresa = "";
	$scope.izdavac.telefon = "";
	
	$scope.totalPages = 1;
	$scope.pageNum = 0;
	var config = {params: {}};

	var getIzdavaci= function(){
		var promise = $http.get(urlIzdavaci, config);
		
		config.params.pageNum = $scope.pageNum;
		
		promise.then(
			function success(res){
				$scope.izdavaci = res.data;
				$scope.totalPages = res.headers("totalPages");
			},
			function error(res){
				alert("Neuspesno dobavljanje izdavaca.");
			}
		);
	}
	
	getIzdavaci();
	
	$scope.doAdd = function(){
		
		$http.post(urlIzdavaci, $scope.newIzdavac).then(
			function success(res){
				$scope.newIzdavac = {};
				getIzdavaci();
			},
			function error(){
				alert("Izdavac nije sacuvan.");
			}
		);
	}
	
	$scope.doDelete = function(id){
		
		var promise = $http.delete(urlIzdavaci + "/" + id);
		promise.then(
			function success(){
				getIzdavaci();
			},
			function error(){
				alert("Neuspesno brisanje izdavaca.");
			}
		);		
	}
	
	$scope.changePage = function(direction){
		$scope.pageNum += direction;
		getIzdavaci();
	}
});


wafepaApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : '/app/html/pocetna.html'
		})
		.when('/knjige', {
			templateUrl : '/app/html/pocetna.html'
		})
		.when('/knjiga/edit/:id', {
			templateUrl : '/app/html/edit-knjiga.html'
		})
		.when('/izdavaci', {
			templateUrl : '/app/html/izdavaci.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);

